from django.shortcuts import render_to_response
from django.template import RequestContext
from tasktracker.views import Task


# можно переписать как в news/views.py
def home(request):
    vars = dict (
            posts=Task.objects.all().order_by('-timechange'),
                )

    return render_to_response('tasktracker/index.html', vars, context_instance=RequestContext(request))
