from django.contrib import messages
from django.utils import timezone
from datetime import datetime

from .models import Task, Reccurentask
from .consts import State

def checkfails(request):
    current_user = request.user
    tasks = Task.objects.filter(author=current_user).order_by('-timechange')
    for task in tasks:
        if task.state == str(State.NOT_READY):
            if task.deadline:
                if task.deadline <= timezone.now():
                    task.state = State.FAILED
                    task.timechange = datetime.now()
                    task.save()
                    messages.warning(request,'Task {} was failed'.format(task))

def checkreccurent(request):
    current_user = request.user
    reccurenttasks = Reccurentask.objects.filter(author=current_user).order_by('-timechange')
    for task in reccurenttasks:
        if task.state == str(State.NOT_READY):
            if task.deadline <= timezone.now():
                newtask = task
                task.delete()
                if task.period:
                    newtask.deadline = newtask.deadline + relativedelta(days=newtask.period)
                else:
                    values = task.get_days_display().split(',')
                    day = task.deadline.weekday()
                    time = str(task.deadline.time())
                    exist = False
                    i = 0
                    for days in task.days:
                        if int(days) > day:
                            day = int(days)
                            exist = True
                            newtask.deadline = dateutil.parser.parse(values[i] + time)
                        i = i + 1
                    if not exist:
                        newtask.deadline = dateutil.parser.parse(values[0] + time)
                newtask.timecreate = datetime.now()
                newtask.timechange = datetime.now()
                newtask.save()
                messages.info(request,'Task {} was repeated'.format(newtask))
