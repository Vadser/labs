from django.conf.urls import url

from . import views

app_name = 'tasktracker'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<task_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^add', views.add, name='add'),
    url(r'^subtask(?P<task_id>[0-9]+)/', views.subtask, name='subtask'),
    url(r'^delete(?P<task_id>[0-9]+)/$', views.delete, name='delete'),
    url(r'^edit(?P<task_id>[0-9]+)/$', views.edit, name='edit'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^complete(?P<task_id>[0-9]+)/$', views.complete, name='complete'),
    url(r'^removeuser/$', views.removeuser, name='removeuser'),
    url(r'^reccurent', views.addreccurenttask, name='reccurent'),
    url(r'^rdelete(?P<task_id>[0-9]+)/$', views.rdelete, name='rdelete'),
    url(r'^redit(?P<task_id>[0-9]+)/$', views.redit, name='redit'),
    url(r'^rdetail(?P<task_id>[0-9]+)/$', views.rdetail, name='rdetail'),
    url(r'^rcomplete(?P<task_id>[0-9]+)/$', views.rcomplete, name='rcomplete'),
    url(r'^show(?P<state>.*)/$', views.show, name='show'),
    url(r'^password/$', views.change_password, name='change_password'),

]
