# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime
from django.contrib.auth.models import User
from django.db import models
from multiselectfield import MultiSelectField

from .consts import DAYS, State

class Task(models.Model):
    title = models.CharField(max_length=150)
    body = models.TextField()
    deadline = models.DateTimeField(null=True, blank=True)
    group = models.CharField(max_length=150)
    priority = models.CharField(max_length=150)
    state = models.CharField(max_length=150, default=State.NOT_READY)
    timechange = models.DateTimeField(default=datetime.now, blank=True)
    timecreate = models.DateTimeField(default=datetime.now, blank=True)
    subtask = models.BooleanField(default=False)
    parent = models.IntegerField(default=0)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    users = models.ManyToManyField(User, related_name='shared_users', default=None, null=True, blank=True)

    def __str__(self):
        return self.title

class Reccurentask(models.Model):
    title = models.CharField(max_length=150)
    body = models.TextField()
    deadline = models.DateTimeField()
    group = models.CharField(max_length=150)
    priority = models.CharField(max_length=150)
    state = models.CharField(max_length=150, default=State.NOT_READY)
    period = models.IntegerField(null=True, blank=True)
    days = MultiSelectField(choices=DAYS, max_choices=7, max_length=14, blank=True, null=True)
    timechange = models.DateTimeField(default=datetime.now, blank=True)
    timecreate = models.DateTimeField(default=datetime.now, blank=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
