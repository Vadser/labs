from enum import Enum

DAYS = ((0, 'Monday'),
        (1, 'Tuesday'),
        (2, 'Wednesday'),
        (3, 'Thursday'),
        (4, 'Friday'),
        (5, 'Saturday'),
        (6, 'Sunday'))

class State(Enum):

    READY = 'Ready'
    NOT_READY = 'Not Ready'
    FAILED = 'Failed'

    def __str__(self):
        return self.value
