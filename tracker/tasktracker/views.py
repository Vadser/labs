# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, logout, update_session_auth_hash
from django.template import RequestContext
from django.contrib import messages
from django.contrib.auth import login as auth_login
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, PasswordChangeForm
from django.shortcuts import get_object_or_404, render, redirect
from django.utils import timezone
from django.http.response import HttpResponseBadRequest
from django.utils.datastructures import MultiValueDictKeyError
import dateutil.parser
from dateutil.relativedelta import relativedelta
from datetime import datetime

from .models import Task, Reccurentask
from .consts import State
from .checks import checkfails, checkreccurent

@login_required(login_url='tasktracker:login')
def detail(request, task_id):
    task = get_object_or_404(Task, pk=task_id, users=request.user)
    return render(request, 'tasktracker/detail.html', {'task': task, 'date': timezone.now()})

@login_required(login_url='tasktracker:login')
def index(request):
    current_user = request.user
    checkfails(request)
    checkreccurent(request)
    reccurenttasks = Reccurentask.objects.filter(author=current_user).order_by('-timechange')
    tasks = Task.objects.filter(author=current_user).order_by('-timechange')
    teamtasks = Task.objects.filter(users=current_user).order_by('-timechange')
    context = {'tasks': tasks, 'reccurenttasks': reccurenttasks, 'teamtasks': teamtasks}
    return render(request, 'tasktracker/index.html', context)

@login_required(login_url='tasktracker:login')
def show(request, state):
    current_user = request.user
    tasks = Task.objects.filter(author=current_user, state=state).order_by('-timechange')
    reccurenttasks = Reccurentask.objects.filter(author=current_user, state=state).order_by('-timechange')
    teamtasks = Task.objects.filter(users=current_user, state=state).order_by('-timechange')
    context = {'tasks': tasks, 'reccurenttasks': reccurenttasks, 'teamtasks': teamtasks}
    return render(request, 'tasktracker/index.html', context)

@login_required(login_url='tasktracker:login')
def add(request):
    users = User.objects.all()
    current_user = request.user
    if request.POST :
        try:
            title = request.POST['title']
            body = request.POST['text']
            try:
                deadline = request.POST['deadline']
                if deadline == '':
                    deadline = None
                else:
                    deadline = dateutil.parser.parse(deadline)
                    if deadline < datetime.now():
                        messages.error(request,'Input date is not correct!')
                        return render(request, 'tasktracker/add.html', {'users': users, 'current_user': current_user})
            except ValueError:
                messages.error(request,'Please input correct format of date!')
                return render(request, 'tasktracker/add.html', {'users': users, 'current_user': current_user})
            group = request.POST['group']
            priority = request.POST['priority']
            list = request.POST.getlist('users')
            task = Task(title=title, body=body, deadline=deadline, group=group, priority=priority,author=current_user)
            task.save()
            for user in users:
                if user.username in list:
                    task.users.add(user)
            task.users.add(current_user)
            task.save()
        except MultiValueDictKeyError:
            return HttpResponseBadRequest('Bad Request')

        return redirect('tasktracker:index')
    else:
        return render(request, 'tasktracker/add.html',{'users': users, 'current_user': current_user})

@login_required(login_url='tasktracker:login')
def delete(request, task_id):
    task = get_object_or_404(Task, pk=task_id, author=request.user)
    if request.POST:
        tasks = Task.objects.filter().order_by('-timechange')
        if not task.subtask:
            for subtask in tasks:
                if subtask.parent == task.id:
                    subtask.delete()
        task.delete()
    return redirect('tasktracker:index')

@login_required(login_url='tasktracker:login')
def edit(request, task_id):
    task = get_object_or_404(Task, pk=task_id, users=request.user)
    users = User.objects.all()
    current_user = request.user
    if request.POST :
        try:
            task.title = request.POST['title']
            task.body = request.POST['text']
            try:
                deadline = request.POST['deadline']
                if deadline == '':
                    task.deadline = None
                else:
                    task.deadline = dateutil.parser.parse(deadline)
                    if task.deadline < datetime.now():
                        messages.error(request, 'Input date is not correct!')
                        context = {'task': task, 'deadline': deadline, 'current_user': current_user, 'users': users}
                        return render(request, 'tasktracker/edit.html', context)
            except ValueError:
                messages.error(request,'Please input correct format of date!')
                context = {'task': task, 'deadline': deadline, 'current_user': current_user, 'users': users}
                return render(request, 'tasktracker/edit.html', context)
            task.group = request.POST['group']
            task.priority = request.POST['priority']
            task.timechange = datetime.now()
            task.state = State.NOT_READY
            list = request.POST.getlist('users')
            task.users.set([])
            for user in users:
                if user.username in list:
                    task.users.add(user)
            task.users.add(current_user)
            task.save()
        except MultiValueDictKeyError:
            return HttpResponseBadRequest('Bad Request')

        return redirect('tasktracker:index')
    else:
        if task.deadline:
            deadline = datetime.strftime(task.deadline,'%m/%d/%Y %I:%M %p')
        else:
            deadline = task.deadline
        context = {'task': task, 'deadline': deadline, 'current_user': current_user, 'users': users}
        return render(request, 'tasktracker/edit.html', context)

@login_required(login_url='tasktracker:login')
def subtask(request, task_id):
    parenttask = get_object_or_404(Task, pk=task_id, users=request.user)
    if request.POST :
        try:
            title = request.POST['title']
            body = request.POST['text']
            try:
                deadline = request.POST['deadline']
                if deadline == '':
                    deadline = None
                else:
                    deadline = dateutil.parser.parse(deadline)
                    if deadline < datetime.now():
                        messages.error(request,'Input date is not correct!')
                        return render(request, 'tasktracker/subtask.html', {'task': parenttask})
            except ValueError:
                messages.error(request,'Please input correct format of date!')
                return render(request, 'tasktracker/subtask.html', {'task': parenttask})
            group = request.POST['group']
            priority = request.POST['priority']
            current_user = request.user
            task = Task(title=title, body=body, deadline=deadline, group=group,
                        priority=priority,author=current_user)
            task.subtask = True
            task.parent = task_id
            task.save()
            task.users.set(parenttask.users.all())
            task.save()
        except MultiValueDictKeyError:
            return HttpResponseBadRequest('Bad Request')

        return redirect('tasktracker:index')
    else:
        return render(request, 'tasktracker/subtask.html', {'task': parenttask})

def signup(request):
    if request.POST:
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            auth_login(request, user)
            return redirect('tasktracker:index')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})


def login(request):
    if request.POST:
        logout(request)
        form = AuthenticationForm(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                auth_login(request, user)
                return redirect('tasktracker:index')
        else:
            messages.error(request,'username or password not correct')
            return render(request, 'login.html', {'form': form})

    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})

def change_password(request):
    if request.POST:
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            return redirect('tasktracker:index')

    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'change_password.html', {'form': form})


@login_required(login_url='tasktracker:login')
def removeuser(request):
    current_user = request.user
    user = get_object_or_404(User, username=current_user)
    user.delete()
    return redirect('tasktracker:index')

@login_required(login_url='tasktracker:login')
def logout_view(request):
    logout(request)
    return redirect('tasktracker:index')

@login_required(login_url='tasktracker:login')
def complete(request, task_id):
    task = get_object_or_404(Task, pk=task_id, users=request.user)
    current_user = request.user
    tasks = Task.objects.filter().order_by('-timechange')
    if not task.subtask:
        for subtask in tasks:
            if subtask.parent == task.id:
                subtask.state = State.READY
                subtask.save()
    task.state = State.READY
    task.save()
    return redirect('tasktracker:index')

@login_required(login_url='tasktracker:login')
def addreccurenttask(request):
    if request.POST :
        try:
            title = request.POST['title']
            body = request.POST['text']
            try:
                deadline = dateutil.parser.parse(request.POST['deadline'])
                if deadline < datetime.now():
                    messages.error(request,'Input date is not correct!')
                    return render(request, 'tasktracker/reccurent.html')
            except ValueError:
                messages.error(request,'Please, input deadline!')
                return render(request, 'tasktracker/reccurent.html')
            group = request.POST['group']
            priority = request.POST['priority']
            try:
                period = int(request.POST['period'])
            except ValueError:
                period = None
            days = request.POST.getlist('weekdays')
            if not period and not days:
                messages.error(request,'Please, enter the correct value of period or choose weekdays!')
                return render(request, 'tasktracker/reccurent.html')
            current_user = request.user
            task =  Reccurentask(title=title, body=body, deadline=deadline, group=group,
                                priority=priority,author=current_user, period=period, days=days)
            task.save()
        except MultiValueDictKeyError:
            return HttpResponseBadRequest('Bad Request')

        return redirect('tasktracker:index')
    else:
        return render(request, 'tasktracker/reccurent.html')

@login_required(login_url='tasktracker:login')
def redit(request, task_id):
    task = get_object_or_404(Reccurentask, pk=task_id, author=request.user)
    if request.POST :
        try:
            task.title = request.POST['title']
            task.body = request.POST['text']
            try:
                task.deadline = dateutil.parser.parse(request.POST['deadline'])
                deadline = datetime.strftime(task.deadline,'%m/%d/%Y %I:%M %p')
                if task.deadline < datetime.now():
                    messages.error(request, 'Input date is not correct!')
                    return render(request, 'tasktracker/redit.html', {'task': task, 'deadline': deadline})
            except ValueError:
                deadline = datetime.strftime(task.deadline,'%m/%d/%Y %I:%M %p')
                messages.error(request,'Please, input deadline!')
                return render(request, 'tasktracker/redit.html', {'task': task, 'deadline': deadline})
            task.group = request.POST['group']
            task.priority = request.POST['priority']
            try:
                task.period = int(request.POST['period'])
            except ValueError:
                task.period = None
            task.days = request.POST.getlist('weekdays')
            if not task.period and not task.days:
                deadline = datetime.strftime(task.deadline,'%m/%d/%Y %I:%M %p')
                messages.error(request,'Please, enter the correct value of period or choose weekdays!')
                return render(request, 'tasktracker/redit.html', {'task': task, 'deadline': deadline})
            task.timechange = datetime.now()
            task.state = State.NOT_READY
            task.save()
        except MultiValueDictKeyError:
            return HttpResponseBadRequest('Bad Request')

        return redirect('tasktracker:index')
    else:
        if task.deadline:
            deadline = datetime.strftime(task.deadline,'%m/%d/%Y %I:%M %p')
        else:
            deadline = task.deadline
        return render(request, 'tasktracker/redit.html', {'task': task, 'deadline': deadline})

@login_required(login_url='tasktracker:login')
def rdelete(request, task_id):
    task = get_object_or_404(Reccurentask, pk=task_id, author=request.user)
    if request.POST:
        current_user = request.user
        task.delete()
    return redirect('tasktracker:index')

@login_required(login_url='tasktracker:login')
def rdetail(request, task_id):
    task = get_object_or_404(Reccurentask, pk=task_id, author=request.user)
    days = task.get_days_display()
    return render(request, 'tasktracker/rdetail.html', {'task': task, 'days': days})

@login_required(login_url='tasktracker:login')
def rcomplete(request, task_id):
    task = get_object_or_404(Reccurentask, pk=task_id, author=request.user)
    current_user = request.user
    task.state = State.READY
    task.save()
    return redirect('tasktracker:index')
