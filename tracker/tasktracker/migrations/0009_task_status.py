# Generated by Django 2.0.4 on 2018-06-20 15:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasktracker', '0008_auto_20180620_1348'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='status',
            field=models.CharField(default='Not Ready', max_length=150),
        ),
    ]
