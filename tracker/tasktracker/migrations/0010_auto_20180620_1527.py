# Generated by Django 2.0.4 on 2018-06-20 15:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tasktracker', '0009_task_status'),
    ]

    operations = [
        migrations.RenameField(
            model_name='task',
            old_name='status',
            new_name='state',
        ),
    ]
