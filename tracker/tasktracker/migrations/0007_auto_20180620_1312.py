# Generated by Django 2.0.4 on 2018-06-20 13:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasktracker', '0006_auto_20180619_2229'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='parent',
            field=models.CharField(default=None, max_length=150),
        ),
        migrations.AddField(
            model_name='task',
            name='subtask',
            field=models.BooleanField(default=False),
        ),
    ]
