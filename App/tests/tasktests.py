import unittest

from tracker.source import Task, Add, Edit, Tracker

class Tasktest(unittest.TestCase):

    def test_input(self):
        tracker = Tracker('config1.ini')
        task = Add.createtask('hjh', False, 'jhj', 'High',False, None, 'jjj', [], '22/04/19', 'jjk')
        self.assertIsInstance(task, Task)

    def test_redact(self):
        tracker = Tracker('config1.ini')
        task = Add.createtask('hjh', False, 'jhj', 'High', False, None, 'jjj', [], '22/04/19', 'jjk')
        task.edit('eww', 'Low', '22/04/19', 'dss', 'dsd')
        self.assertEqual(task.priority, 'Low')

    def test_remove(self):
        tasklist = []
        tasklist.append(Add.createtask('hjh', False, 'jhj', 'High', False, None, 'jjj', [], '22/04/19', 'jjk'))
        self.assertEqual(len(tasklist), 1)
        tasklist, exist = Edit.removefind('hjh', tasklist, False)
        self.assertEqual(len(tasklist), 0)
        self.assertEqual(exist, True)

    def test_redactlist(self):
        tasklist = []
        tasklist.append(Add.createtask('hjh', False, 'jhj', 'High', False, None, 'jjj', [], '22/04/19', 'jjk'))
        tasklist[0].edit('eww', 'Low', '22/04/19', 'dss', 'dsd')
        tasklist, exist = Edit.editfind('hjh', 'dsd', 'eww', 'Low', '22/04/19', 'dss', tasklist, False)
        self.assertEqual(tasklist[0].name, 'dsd')

if __name__ == '__main__':
    unittest.main()
