import unittest

from tracker.source import Task, Add, Edit, Tracker

class Tasktest(unittest.TestCase):

    def test_reccurent(self):
        tracker = Tracker('config1.ini')
        output = tracker.checkreccurent()
        self.assertFalse(output is '' + 'Reccurent task {} was repeated!'.format('sds') + '\n' )

    def test_checkfails(self):
        tracker = Tracker('config1.ini')
        output = tracker.checkfails()
        self.assertFalse(output is '' + 'Task {} was failed!'.format('sdf') + '\n')

if __name__ == '__main__':
    unittest.main()
