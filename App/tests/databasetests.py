import unittest

from tracker.source import Task, Add, Edit, Tracker, Datab, User, Userf

class Tasktest(unittest.TestCase):

    def test_input(self):
        tracker = Tracker('config1.ini')
        userlist = []
        user = User('sds',[])
        task = Add.createtask('hjh', False, 'jhj', 'High',True, 6, 'jjj', [], '22/04/19', 'jjk')
        user.tasks.append(task)
        userlist.append(user)
        Datab.serializejson(userlist, 'data.json')
        userlist1 = Datab.deserializejson('data.json')
        self.assertEqual(userlist[0].name, userlist1[0].name)

    def test_load(self):
        tracker = Tracker('config1.ini')
        task = Add.createtask('hjh', False, 'jhj', 'High',True, 6, 'jjj', [], '22/04/19', 'jjk')
        user.tasks.append(task)
        userlist.append(user)
        Datab.serializejson(userlist, 'data.json')
        tasklist, teamtasklist, userlist = Datab.load(tracker.config['Default']['database'], tracker.user)
        self.assertEqual(tasklist[0].name, 'hjh')
        self.assertEqual(tracker.user, 'sds')

    def test_save(self):
        tracker = Tracker('config1.ini')
        tasklist = []
        tasklist1 = []
        task = Add.createtask('sd', False, 'jhj', 'High',False, None, 'jjj', [], '22/04/19', 'jjk')
        tasklist.append(task)
        Datab.save(tasklist, tasklist1, tracker.config['Default']['database'], tracker.user, tracker.userlist)
        tasklist, tasklist1, userlist = Datab.load(tracker.config['Default']['database'], tracker.user)
        self.assertEqual(tasklist[0].name, task.name)

if __name__ == '__main__':
    unittest.main()
