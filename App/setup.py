from setuptools import setup

setup(name='tracker',
      packages=['tracker','tracker.source'],
      entry_points={
          'console_scripts': [
              'tracker = tracker.menu:main'
          ]
      },
      )
