import json
import configparser
import jsonpickle
import os

class Datab:
    """Perform functions to work with our database:
        serialize_json - serialize our tasks to json.
        deserialize_json - deserialize our tasks from json.
    """

    @staticmethod
    def currentuser(config):
        """
        Return current user
        """
        
        try:
            with open(config['Default']['user'], 'r') as file:
                username = json.load(file)
            return username
        except Exception:
            return ' '
            raise NameError('Create user!')

    @staticmethod
    def configparse(configfile):
        """
        Return content of configfile
        """

        if os.path.exists(configfile):
            config = configparser.ConfigParser()
            config.read(configfile)
            return config
        else:
            raise IOError('No such {} configfile'.format(configfile))

    @staticmethod
    def serializejson(instance=None, path=None):
        json_obj = jsonpickle.encode(instance)
        with open(path, 'w') as file:
            file.write(json_obj)

    @staticmethod
    def deserializejson(path=None):
        if os.stat(path).st_size == 0:
            Datab.serializejson([], path)
        with open(path, 'r') as file:
            json_str = file.read()
        userlist = jsonpickle.decode(json_str)
        return userlist

    @staticmethod
    def load(path, username):
        """
        Load tasklist of current user and tasks for which current user have access
        """

        tasklist = []
        teamtasklist = []
        userlist = Datab.deserializejson(path)
        for user in userlist:
            if user.name == username:
                tasklist = user.tasks
        for user in userlist:
            tasks = []
            for task in user.tasks:
                if username in task.users:
                    teamtasklist.append(task)
                    tasks.append(task)
            for task in tasks:
                user.tasks.remove(task)
        return tasklist, teamtasklist, userlist

    @staticmethod
    def save(tasklist, teamtasklist, path, username, userlist):
        """
        Save tasklist of current user and tasks for which current user have access
        """

        for user in userlist:
            if user.name == username:
                user.tasks = tasklist
        for user in userlist:
            for task in teamtasklist:
                if user.name == task.user:
                    user.tasks.append(task)
        Datab.serializejson(userlist, path)
