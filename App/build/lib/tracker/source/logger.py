import logging

class Logger:
    """Class to create logger"""

    @staticmethod
    def createlog(config):
        logging.basicConfig(filename=config['Default']['logging'], level=logging.INFO,
                            format='%(asctime)s | %(levelname)s | %(message)s', datefmt='%m/%d/%Y %H:%M:%S')
        log = logging.getLogger('ex')
        if config['Default']['logger'] == 'OFF':
            log.info('OFF')
            logging.disable(logging.CRITICAL)
        return log
