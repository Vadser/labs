import json

from tracker.source.user import User

class Userf:
    """Perfoms fuctions to work with our users:
        createuser - create user.
        changeuser - change current user.
        printuser - print all users.
        deleteuser - delete user.
    """

    @staticmethod
    def createuser(userlist, userlistct, login, log, config):
        exist = False
        for user in userlist:
            if user.name == login:
                exist = True
        if exist:
            raise NameError('User with such {} name exist'.format(login))
        newuser = User(login,[])
        userlist.append(newuser)
        userlistct.append(newuser)
        with open(config['Default']['user'], 'w') as file:
            json.dump(login, file)
        log.info('New user with name {} was created!'.format(login))
        return userlist, userlistct

    @staticmethod
    def changeuser(userlist, login, log, config):
        exist = False
        for user in userlist:
            if user.name == login:
                with open(config['Default']['user'], 'w') as file:
                    json.dump(login, file)
                exist = True
        log.info('Try to change user')
        if not exist:
            raise NameError('No such user - {}'.format(login))
        else:
            log.info('User {} was successfully changed'.format(login))

    @staticmethod
    def printuser(userlist, log, config):
        output = ''
        log.info('Print users')
        try:
            output += 'Now:' + '\n'
            with open(config['Default']['user'], 'r') as file:
                username = json.load(file)
            output += username + '\n'
            output += 'All:' + '\n'
            for user in userlist:
                output += user.name + '\n'
        except Exception:
            output += 'No users!'
            log.info('No users')
        return output

    @staticmethod
    def deleteuser(userlist, userlistct, login, log, config):
        output += ''
        exist = False
        for user in userlist:
            if user.name == login:
                userlist.remove(user)
                exist = True;
                break
        for user in userlistct:
            if user.name == login:
                userlistct.remove(user)
        log.info('Try to remove user')
        try:
            with open(config['Default']['user'], 'r') as file:
                user = json.load(file)
            if exist:
                log.info('User {} was successfully removed!'.format(login))
            else:
                raise NameError('No such user - {}!'.format(login))
        except Exception:
            raise NameError('No users!')
        try:
            if user == login:
                with open(config['Default']['user'], 'w') as file:
                    json.dump(userlist[0].name, file)
        except Exception:
            with open(config['Default']['user'], 'w') as file:
                json.dump('', file)
            output += 'The last user was removed!'
            log.info('The last user was removed!')
        return userlist, userlistct, output
