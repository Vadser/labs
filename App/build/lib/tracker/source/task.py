from datetime import datetime, date
import json

from tracker.source.state import State

class Task:
    """Describe our tasks"""

    def __init__(self, name, list, deadline, state, subtasks, subtask, create,
                 change, group, priority, reccurent, days, user, users):
        self.name = name
        self.list = list
        self.deadline = deadline
        self.state = state
        self.subtasks = subtasks
        self.subtask = subtask
        self.create = create
        self.change = change
        self.group = group
        self.priority = priority
        self.reccurent = reccurent
        self.days = days
        self.user = user
        self.users = users

    def __str__(self):
        result = []
        if not self.subtask:
            result.append('Task: ')
        result.append('Name: {}'.format(self.name))
        if self.list is not None:
            result.append('To-do list: {}'.format(self.list))
        if self.deadline is not None:
            result.append('Deadline: {}'.format(self.deadline))
        result.append('State: {}'.format(self.state))
        result.append('Create: {}'.format(self.create))
        result.append('Change: {}'.format(self.change))
        if self.group is not None:
            result.append('Group: {}'.format(self.group))
        if self.priority is not None:
            result.append('Priority: {}'.format(self.priority))
        if self.reccurent:
            if isinstance(self.days,str):
                result.append('Repeat each {} days'.format(self.days))
            else:
                result.append('Repeat at {}'.format([str(day) for day in self.days]))
        if not len(self.subtasks) == 0:
            result.append('Subtasks:')
            for task in self.subtasks:
                result.append(str(task))
        return '\n'.join(result)

    def edit(self, group, priority, deadline, list, newname):
        """
        Function edit our task
        """

        if newname is not None:
            self.name = newname
        if group is not None:
            self.group = group
        if priority is not None:
            self.priority = priority
        if deadline is not None:
            self.deadline = deadline
            self.state = State.NOT_READY
        if list is not None:
            self.list = list
        self.change = datetime.now()
