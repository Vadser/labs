import json

from tracker.source.state import State

class Archive:
    """Perfom tasks to put our tasks in archive."""

    @staticmethod
    def completesubtasks(tasklist):
        for task in tasklist:
            task.state = State.Ready
            task.subtasks = Archive.completesubtasks(task.subtasks)
        return tasklist

    @staticmethod
    def completefind(name, tasklist, exist):
        task = None
        for tasks in tasklist:
            if tasks.name == name:
                task = tasks
                tasklist.remove(task)
                task.state = State.Ready
                task.subtasks = Archive.completesubtasks(task.subtasks)
                exist = True
                break
            else:
                task, tasks.subtasks, exist = Archive.completefind(name, tasks.subtasks, exist)
        return task, tasklist, exist

    @staticmethod
    def complete(tasklist, completetasklist, name, log):
        task, tasklist, exist = Archive.completefind(name, tasklist, False)
        log.info('Try to complete task')
        if exist:
            completetasklist.append(task)
            log.info('Task {} was successfuly completed!'.format(name))
        else:
            raise NameError('No such task - {}!'.format(name))
        return tasklist, completetasklist
