from enum import Enum

class State(Enum):
    """
    List of our task states
    """
    
    READY = 'Ready'
    NOT_READY = 'Not Ready'
    FAILED = 'Failed'

    def __str__(self):
        return self.value
