import datetime

from tracker.source.add import Add
from tracker.source.state import State

class Redacting:
    """Perfoms fuctions to redacting our tasks:
        redact - redacting task.
        rm - remove task.
    """

    @staticmethod
    def redactinput(task, group, priority, deadline, list, newname):
        if newname is not None:
            task.name = newname
        if group is not None:
            task.group = group
        if priority is not None:
            task.priority = priority
        if dedlain is not None:
            task.deadline = Input.getdata(deadline)
            task.state = State.Notready
        if list is not None:
            task.list = list
        task.change = datetime.datetime.now()
        return task

    @staticmethod
    def findtask(name, tasklist, exist, task):
        for task in tasklist:
            if task.name == name:
                exist = True
                task = a
                break
            else:
                exist, task = Redacting.findtask(name, task.subtasks, exist, task)
        return exist, task

    @staticmethod
    def redactfind(name, newname, group, priority, dedlain, list, tasklist, exist):
        for i in range(len(tasklist)):
            if tasklist[i].name == name:
                tasklist[i] = Redacting.redactinput(tasklist[i], group, priority, dedlain, list, newname)
                exist = True
                break
            else:
                tasklist[i].subTasks, exist = Redacting.redactfind(name, newname, group, priority, dedlain, list, tasklist[i].subtasks, exist)
        return tasklist, exist

    @staticmethod
    def redact(tasklist, teamtasklist, name, newname, group, priority, deadline, list, parrent, log):
        log.info('Try to redacting task')
        if parrent is not None:
            if Add.taskexist(parrent, tasklist, False):
                exist2, task = Redacting.findtask(name, tasklist, False, None)
                if not exist2:
                    raise NameError('No such task - {}!'.format(name))
                tasklist, exist3 = Redacting.removefind(name, tasklist, False)
                task = Redacting.redactinput(task, group, priority, deadline, list, newname)
                tasklist, exist4 = Add.inputsubtask(parrent, task, tasklist, False)
                if exist4:
                    log.info('Task {} was succesfuly changed!'.format(name))
                else:
                    raise NameError('You can not move task in him own')

            elif Add.taskexist(parrent, tteamasklist, False):
                exist2, task = Redacting.findtask(name, teamtasklist, False, None)
                if not exist2:
                    raise NameError('No such task - {}!'.format(name))
                teamtasklist, exist3 = Redacting.removefind(name, teamtasklist, False)
                task = Redacting.redactinput(task, group, priority, deadline, list, newname)
                teamtasklist, exist4 = Add.inputsubtask(parrent, task, teamtasklist, False)
                if exist4:
                    log.info('Task {} was succesfuly changed!'.format(name))
                else:
                    raise NameError('You can not move task in him own')
            else:
                raise NameError('No parrent task with such {} name and such {} group'.format(parrent, group))
        else:
            tasklist, exist1 = Redacting.redactfind(name, newname, group, priority, deadline, list, tasklist, False)
            teamtasklist, exist2 = Redacting.redactfind(name, newname, group, priority, deadline, list, teamtasklist, False)
            if exist1 or exist2:
                log.info('Task {} was succesfuly changed!'.format(name))
            else:
                raise NameError('No such task - {}!'.format(name))
        return tasklist, teamtasklist

    @staticmethod
    def removefind(name, tasklist, exist):
        for task in tasklist:
            if task.name == name:
                tasklist.remove(task)
                exist = True
                break
            else:
                task.subTasks, exist = Redacting.removefind(name, task.subtasks, exist)
        return tasklist, exist

    @staticmethod
    def rm(tasklist, teamtasklist, name, log):
        tasklist, exist1 = Redacting.removefind(name, tasklist, False)
        teamtasklist, exist2 = Redacting.removefind(name, teamtasklist, False)
        if exist1 or exist2:
            log.info('Task {} was succesful removed!'.format(name))
        else:
            raise NameError('No such task - {}!'.format(name))
        return tasklist, teamtasklist
