import json

from tracker.source.datab import Datab
from tracker.source.user import User

class Load:
    """Perform function to load our tasks"""

    @staticmethod
    def load():
        config = Datab.configparse()
        try:
            tasklist = []
            tasklist1 = []
            with open(config['Default']['user'], 'r') as file:
                username = json.load(file)
            userlist = Datab.deserializejson(config['Default']['database'])
            for a in userlist:
                if a.name == username:
                    tasklist = a.tasks
            for a in userlist:
                for b in a.tasks:
                    if username in b.users:
                        tasklist1.append(b)
                        a.tasks.remove(b)
            Datab.serializejson(userlist, config['Default']['database'])
            return tasklist, username, tasklist1
        except Exception:
            raise NameError('Create user!')
