import json

from tracker.source.state import State

class Completetask:
    """Perfom tasks to put our tasks in archive."""

    @staticmethod
    def completesubtasks(tasklist):
        """
        Function change state in subtasks of of our completetask
        """
        
        for task in tasklist:
            task.state = State.READY
            task.subtasks = Archive.completesubtasks(task.subtasks)
        return tasklist

    @staticmethod
    def completefind(name, tasklist, exist):
        """
        Find task and complete it
        """

        task = None
        removing = False
        for tasks in tasklist:
            if tasks.name == name:
                task = tasks
                task.state = State.READY
                task.subtasks = Archive.completesubtasks(task.subtasks)
                exist = True
                removing = True
                break
            else:
                task, tasks.subtasks, exist = Archive.completefind(name, tasks.subtasks, exist)
        if removing:
            tasklist.remove(task)
        return task, tasklist, exist

    @staticmethod
    def complete(tasklist, completetasklist, name, log):
        """
        Compelete task and put it in list of completetasks
        """

        task, tasklist, exist = Archive.completefind(name, tasklist, False)
        log.info('Try to complete task')
        if exist:
            completetasklist.append(task)
            log.info('Task {} was successfully completed!'.format(name))
        else:
            raise NameError('No such task - {}!'.format(name))
        return tasklist, completetasklist
