import datetime
from dateutil.parser import *

from tracker.source.task import Task
from tracker.source.load import Load
from tracker.source.save import Save
from tracker.source.logger import Logger

class Input:
    """Perform function to create our task"""

    @staticmethod
    def getdata(userIn):
        isValid = False
        try:
            date1 = parse(userIn)
            day = datetime.datetime.now()
            isValid = True
        except Exception:
            raise NameError('Incorrect input of data!')
        if date1<day:
            raise NameError('Wrong data!')
        return date1

    @staticmethod
    def input1(name, subtask, group, priority, reccurent, days, user, users, dedlain, list):
        subTasks = []
        create = str(datetime.datetime.now())
        change = str(datetime.datetime.now())
        if dedlain is not None:
            dedlain = str(Input.getdata(dedlain))
        state = 'Not ready'
        return Task(name, list, dedlain, state, subTasks, subtask, create, change, group, priority, reccurent, days, user, users)

    @staticmethod
    def inputsubtask(name, task, tasklist, exist):
        for a in tasklist:
            if a.name == name and a.group == task.group:
                a.subtasks.append(task)
                exist = True
                break
            else:
                a.subtasks, exist = Input.inputsubtask(name, task, a.subtasks, exist)
        return tasklist, exist

    @staticmethod
    def taskexist(name, tasklist, exist):
        for a in tasklist:
            if a.name == name:
                exist = True
                break
            else:
                exist = Input.taskexist(name, a.subtasks, exist)
        return exist

    @staticmethod
    def create(name, group, priority, days, users, parent, dedlain, list):
        log = Logger.createlog()
        reccurent = False
        if days is not None:
            reccurent = True
            if dedlain is None:
                raise NameError('Deadline at reccurent task can not be None!')
            if not days.isdigit():
                days = [Input.getdata(a) for a in days.split(',')]
                days.sort()
        tasklist, user, tasklist1 = Load.load()
        if Input.taskexist(name, tasklist, False) or Input.taskexist(name, tasklist1, False) :
            raise NameError('Task with such {} name exist'.format(name))
        else:
            users1 = []
            if users is not None:
                users1 = users.split(',')
            if parent is not None:
                task = Input.input1(name, True, group, priority, reccurent, days, user, users1, dedlain, list)
                tasklist, exist = Input.inputsubtask(parent, task, tasklist, False)
                tasklist1, exist1 = Input.inputsubtask(parent, task, tasklist1, False)
                if exist or exist1:
                    log.info('Create new task {}'.format(name))
                else:
                    raise NameError('No parrent task with such {} name and such {} group'.format(parent, group))
            else:
                tasklist.append(Input.input1(name, False, group, priority, reccurent, days, user, users1, dedlain, list))
                log.info('Create new task {}'.format(name))
        Save.save(tasklist, tasklist1)
