import datetime

from tracker.source.add import Add
from tracker.source.state import State

class Edit:
    """
    Perfoms fuctions to redacting our tasks:
    """

    @staticmethod
    def findtask(name, tasklist, exist, task):
        """
        Find task which have such nsme
        """

        for tasks in tasklist:
            if tasks.name == name:
                exist = True
                task = tasks
                break
            else:
                exist, task = Edit.findtask(name, tasks.subtasks, exist, task)
        return exist, task

    @staticmethod
    def editfind(name, newname, group, priority, dedlain, list, tasklist, exist):
        """
        Search task which we want to edit and if it exist edit it
        """

        for task in tasklist:
            if task.name == name:
                task.edit(group, priority, dedlain, list, newname)
                exist = True
                break
            else:
                task.subTasks, exist = Edit.editfind(name, newname, group, priority, dedlain, list, task.subtasks, exist)
        return tasklist, exist

    @staticmethod
    def edittask(tasklist, teamtasklist, name, newname, group, priority, deadline, list, parrent, log):
        """
        Edit task in our tasklist and replace it if it need
        """

        log.info('Try to Edit task')
        if deadline is not None:
            deadline = Add.getdate(deadline)
        if parrent is not None:
            if Add.taskexist(parrent, tasklist, False):
                exist2, task = Edit.findtask(name, tasklist, False, None)
                if not exist2:
                    raise NameError('No such task - {}!'.format(name))
                tasklist, exist3 = Edit.removefind(name, tasklist, False)
                task.edit(group, priority, deadline, list, newname)
                tasklist, exist4 = Add.inputsubtask(parrent, task, tasklist, False)
                if not exist4:
                    raise NameError('You can not move the task to itself')
                log.info('Task {} was successfully changed!'.format(name))
            elif Add.taskexist(parrent, teamtasklist, False):
                exist2, task = Edit.findtask(name, teamtasklist, False, None)
                if not exist2:
                    raise NameError('No such task - {}!'.format(name))
                teamtasklist, exist3 = Edit.removefind(name, teamtasklist, False)
                task.edit(group, priority, deadline, list, newname)
                teamtasklist, exist4 = Add.inputsubtask(parrent, task, teamtasklist, False)
                if not exist4:
                    raise NameError('You can not move the task to itself')
                log.info('Task {} was succesfully changed!'.format(name))
            else:
                raise NameError('No parent task with such {} name and such {} group'.format(parrent, group))
        else:
            if Add.taskexist(newname, tasklist, False) or Add.taskexist(newname, teamtasklist, False):
                raise NameError('Task with such {} name exist'.format(newname))
            tasklist, exist1 = Edit.editfind(name, newname, group, priority, deadline, list, tasklist, False)
            teamtasklist, exist2 = Edit.editfind(name, newname, group, priority, deadline, list, teamtasklist, False)
            if exist1 or exist2:
                log.info('Task {} was successfully changed!'.format(name))
            else:
                raise NameError('No such task - {}!'.format(name))
        return tasklist, teamtasklist

    @staticmethod
    def removefind(name, tasklist, exist):
        """
        Search for task which we want to remove and if it exist remove it
        """

        removing = False
        for task in tasklist:
            if task.name == name:
                removabletask = task
                exist = True
                removing = True
                break
            else:
                task.subTasks, exist = Edit.removefind(name, task.subtasks, exist)
        if removing:
            tasklist.remove(removabletask)
        return tasklist, exist

    @staticmethod
    def removetask(tasklist, teamtasklist, name, log):
        """
        Remove task if it exist and return new tasklist
        """
        
        tasklist, exist1 = Edit.removefind(name, tasklist, False)
        teamtasklist, exist2 = Edit.removefind(name, teamtasklist, False)
        if exist1 or exist2:
            log.info('Task {} was successfully removed!'.format(name))
        else:
            raise NameError('No such task - {}!'.format(name))
        return tasklist, teamtasklist
