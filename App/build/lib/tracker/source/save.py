import json

from tracker.source.datab import Datab
from tracker.source.user import User


class Save:
    """Perfoms function to save our task"""

    @staticmethod
    def save(tasklist,tasklist1):
        config = Datab.configparse()
        username = ''
        try:
            with open(config['Default']['user'], 'r') as file:
                username = json.load(file)
        except Exception:
            raise NameError('Create user!')
        userlist = Datab.deserializejson(config['Default']['database'])
        for i in range(len(userlist)):
            if userlist[i].name == username:
                userlist[i].tasks = tasklist
        for a in userlist:
            for b in tasklist1:
                if a.name == b.user:
                    a.tasks.append(b)
        Datab.serializejson(userlist, config['Default']['database'])
