import datetime
from dateutil.parser import *

from tracker.source.state import State
from tracker.source.task import Task

class Add:
    """
    Perform function to create our task
    """

    @staticmethod
    def getdate(userin):
        """
        Function to parse our date and checks for correctness
        """
        
        try:
            date = parse(userin)
            datenow = datetime.datetime.now()
        except Exception:
            raise ValueError('Incorrect input of date!')
        if date<datenow:
            raise ValueError('Wrong date!')
        return date

    @staticmethod
    def createtask(name, subtask, group, priority, reccurent, days, user, users, deadline, list):
        """
        Function to create new task
        """

        subtasks = []
        create = datetime.datetime.now()
        change = datetime.datetime.now()
        if deadline is not None:
            deadline = Add.getdate(deadline)
        state = State.NOT_READY
        return Task(name, list, deadline, state, subtasks, subtask, create,
                    change, group, priority, reccurent, days, user, users)

    @staticmethod
    def inputsubtask(name, task, tasklist, exist):
        """
        Inserts subtask into the right place
        """

        for tasks in tasklist:
            if tasks.name == name and tasks.group == task.group:
                tasks.subtasks.append(task)
                exist = True
                break
            else:
                tasks.subtasks, exist = Add.inputsubtask(name, task, tasks.subtasks, exist)
        return tasklist, exist

    @staticmethod
    def taskexist(name, tasklist, exist):
        """
        Checks if task with such name exist
        """

        for task in tasklist:
            if task.name == name:
                exist = True
                break
            else:
                exist = Add.taskexist(name, task.subtasks, exist)
        return exist

    @staticmethod
    def addtask(tasklist, teamtasklist, name, group, priority, days, users, parent, deadline, list, log, user):
        """
        Add task to our tasklist
        """

        if Add.taskexist(name, tasklist, False) or Add.taskexist(name, teamtasklist, False) :
            raise NameError('Task with such {} name exist'.format(name))
        reccurent = False
        if days is not None:
            reccurent = True
            if deadline is None:
                raise ValueError('Deadline at reccurent task can not be None!')
            if not days.isdigit():
                days = [Add.getdate(day) for day in days.split(',')]
                days.sort()
        userslist = []
        if users is not None:
            userslist = users.split(',')
        if parent is not None:
            task = Add.createtask(name, True, group, priority, reccurent, days, user, userslist, deadline, list)
            tasklist, exist1 = Add.inputsubtask(parent, task, tasklist, False)
            teamtasklist, exist2 = Add.inputsubtask(parent, task, teamtasklist, False)
            if exist1 or exist2:
                log.info('Create new task {}'.format(name))
            else:
                raise NameError('No parent task with such {} name and such {} group'.format(parent, group))
        else:
            tasklist.append(Add.createtask(name, False, group, priority, reccurent, days, user, userslist, deadline, list))
            log.info('Create new task {}'.format(name))
        return tasklist, teamtasklist
