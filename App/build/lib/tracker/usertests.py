import unittest

from source import User,Task

class TestUser(unittest.TestCase):

    def test_create(self):
        user = User('sds',[])
        self.assertIsInstance(user, User)

    def test_add_task(self):
        user = User('sds',[])
        task = Task('sd','dsd','22/01/19','Ready',[],False,'22/01/19','22/01/19','dsd','Low',False,5,'sds',[])
        user.tasks.append(task)
        self.assertEquals(len(user.tasks), 1)
        self.assertIs(user.tasks[0], task)

    def test_task_change(self):
        user = User('sds',[])
        task = Task('sd','dsd','22/01/19','Ready',[],False,'22/01/19','22/01/19','dsd','Low',False,5,'sds',[])
        user.tasks.append(task)
        user.tasks[0].list = 'fgh'
        self.assertEquals(user.tasks[0].list, 'fgh')


if __name__ == '__main__':
    unittest.main()
