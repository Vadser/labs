Tracker
====
Good program for easy manage you work
It will help you to keep track of your affairs

Installing
====
Step one:
Move to App directory
<cd .../App>
Step two:
Run setup.py
<python3 setup.py install>

Running the tests
====
For running tests move to tests folder
<cd .../App/tests>
And run tests
<python3 nameoftests.py>

Functionality
====
You can create, edit and remove your own tasks.
You can create periodic tasks, which will repeat in period that your want.

Begin work
====
If you want to begin to work with application, open terminal and enter one of this command:
tracker task create – create new task
tracker task edit – change the task
tracker task remove – remove the task
tracker task complete – complete the task
tracker user create – create new user
tracker user change – change current user
tracker user print – print all users
tracker user remove – remove user
tracker – print all tasks of current user
tracker -less – print only names of tasks
If you want to get help – enter:
tracker --help or tracker -h

Author
====
Made by Vadim Serebrennikov
