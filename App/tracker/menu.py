#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import datetime

from tracker.source import Tracker

def required_value(one,two,three):
    class RequiredArgument(argparse.Action):
        def __call__(self, parser, namespace, value, option_string=None):
            if  not value == one and not value == two and not value == three :
                raise Exception('Wrong format. Argument -p value must be High, Medium or Low')
            setattr(namespace, self.dest, value)
    return RequiredArgument

tracker = Tracker('config.ini')

def printtasks(args):
    print(tracker.printtasks(args.p, args.g, args.n, args.flag_exists))

def createtask(args):
    tracker.createtask(args.name, args.g, args.p, args.r, args.u, args.pr, args.d, args.t)

def removetask(args):
    tracker.removetask(args.name)

def edittask(args):
    tracker.edittask(args.name, args.n, args.g, args.p, args.d, args.t, args.pr)

def createusers(args):
    tracker.createusers(args.login)

def changeusers(args):
    tracker.changeusers(args.name)

def printusers(args):
    print(tracker.printusers())

def removeuser(args):
    print(tracker.removeuser(args.name))

def completetask(args):
    tracker.completetask(args.name)

def main():
    log = tracker.logger
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('-less', dest='flag_exists', action='store_true',help='print less information')
        parser.add_argument('-n',type=str,help='name of task which you want to see')
        parser.add_argument('-g',type=str,help='group name')
        parser.add_argument('-p',type=str,action=required_value('High','Medium','Low'),help='task priority (High,Medium,Low)')
        parser.set_defaults(func = printtasks)
        subparsers = parser.add_subparsers()

        task = subparsers.add_parser('task',help='work with tasks')
        tasksubparsers = task.add_subparsers()
        createp = tasksubparsers.add_parser('create',help='create task')
        createp.add_argument('name',type=str,help='task name',default='New Task')
        createp.add_argument('-g',type=str,help='task group')
        createp.add_argument('-p',type=str,action=required_value('High','Medium','Low'),help='task priority (High,Medium,Low)')
        createp.add_argument('-r',type=str,help='reccurent task(write after how many days to repeat)')
        createp.add_argument('-u',type=str,help='enter user for which open access to the task(insert example: user1,user2)')
        createp.add_argument('-pr',type=str,help='parent task name')
        createp.add_argument('-d',type=str,help='dedlain(type of input dd/mm/yy)')
        createp.add_argument('-t',type=str,help='to-do list')
        createp.set_defaults(func=createtask)
        rmp = tasksubparsers.add_parser('remove',help='remove task')
        rmp.add_argument('name',type=str,help='task name')
        rmp.add_argument('-p',type=str,help='task parent')
        rmp.set_defaults(func=removetask)
        redactp = tasksubparsers.add_parser('change',help='redact task')
        redactp.add_argument('name',type=str,help='task name')
        redactp.add_argument('-n',type=str,help='new name')
        redactp.add_argument('-g',type=str,help='task group')
        redactp.add_argument('-p',type=str,action=required_value('High','Medium','Low'),help='task priority (High,Medium,Low)')
        redactp.add_argument('-d',type=str,help='dedlain')
        redactp.add_argument('-t',type=str,help='to-do list')
        redactp.add_argument('-pr',type=str,help='new parent')
        redactp.set_defaults(func=edittask)
        completet = tasksubparsers.add_parser('complete',help='complete task')
        completet.add_argument('name',type=str,help='task name')
        completet.set_defaults(func=completetask)

        user = subparsers.add_parser('user',help='work with users')
        usersubparsers = user.add_subparsers()
        createuser = usersubparsers.add_parser('create',help='create user')
        createuser.add_argument('login',type=str,help='user login')
        createuser.set_defaults(func=createusers)
        changeuser = usersubparsers.add_parser('change',help='change user')
        changeuser.add_argument('name',type=str,help='user login')
        changeuser.set_defaults(func=changeusers)
        printuser = usersubparsers.add_parser('print',help='print users')
        printuser.set_defaults(func=printusers)
        rmuser = usersubparsers.add_parser('remove',help='remove users')
        rmuser.add_argument('name',type=str,help='user login')
        rmuser.set_defaults(func=removeuser)

        args = parser.parse_args()
        args.func(args)
        print(tracker.checkreccurent())
        print(tracker.checkfails())
        tracker.saveall()
    except Exception as e:
        print(str(e))
        log.error(str(e))
        raise SystemExit(1)

if __name__ == '__main__':
    main()
