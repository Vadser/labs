class Prints:
    """Perform fuctions to show our tasks"""

    @staticmethod
    def printless(s, tasklist, output):
        """
        Print only names of our tasks
        """

        s += ' '
        for task in tasklist:
            output.append(s + task.name)
            output = Prints.printless(s, task.subtasks, output)
        return output

    @staticmethod
    def printtask(tasklist, teamtasklist, completetasklist, priority, group, name, less, log):
        """
        Print our tasks:
        less - print only names
        print tasks with specified priority or group
        """

        output = []
        if less:
            output.append('Tasks:')
            output.append('\n'.join(Prints.printless('', tasklist, [])))
            output.append('Task for which have access:')
            log.info('Print Tasks')
            output.append('\n'.join(Prints.printless('', teamtasklist, [])))
        else:
            if name is not None:
                check = False
                for task in tasklist:
                    if task.name == name:
                        output.append(str(task))
                        check = True
                for task in teamtasklist:
                    if task.name == name:
                        output.append(str(task))
                        check = True
                log.info('Print Task {}'.format(name))
                if not check:
                    output.append('There is no task with such name - {}'.format(name))
                    log.info('There is no task with such name - {}'.format(name))
            else:
                if priority is not None and group is not None:
                    check = False
                    output.append('Tasks:')
                    for task in tasklist:
                        if task.group == group and task.priority == priority:
                            output.append(str(task))
                            check = True
                    output.append('Task for which have access:')
                    for task in teamtasklist:
                        if task.group == group and task.priority == priority:
                            output.append(str(task))
                            check = True
                    log.info('Print Tasks')
                    if not check:
                        output.append('There is no such task that would satisfy all the requirements!')
                        log.info('There is no such task that would satisfy all the requirements!')
                else:
                    if group is not None:
                        if group == 'complete':
                            check = False
                            output.append('Tasks:')
                            for task in completetasklist:
                                output.append(str(task))
                                check = True
                            log.info('Print Tasks')
                            if not check:
                                output.append('No Complete tasks!')
                                log.info('No Complete tasks!')
                        elif group == 'Failed':
                            check = False
                            output.append('Tasks:')
                            for task in tasklist:
                                if task.state == group:
                                    output.append(str(task))
                                    check = True
                            output.append('Task for which have access:')
                            for task in teamtasklist:
                                if task.state == group:
                                    output.append(str(task))
                                    check = True
                            log.info('Print Tasks')
                            if not check:
                                output.append('No Failed tasks!')
                                log.info('No Failed tasks!')
                        else:
                            check = False
                            output.append('Tasks:')
                            for task in tasklist:
                                if task.group == group:
                                    output.append(str(task))
                                    check = True
                            output.append('Task for which have access:')
                            for task in teamtasklist:
                                if task.group == group:
                                    output.append(str(task))
                                    check = True
                            log.info('Print Tasks')
                            if not check:
                                output.append('No such group - {}!'.format(group))
                                log.info('No such group - {}!'.format(group))
                    elif priority is not None:
                        check = False
                        output.append('Tasks:')
                        for task in tasklist:
                            if task.priority == priority:
                                output.append(str(task))
                                check = True
                        output.append('Task for which have access:')
                        for task in teamtasklist:
                            if task.priority == priority:
                                output.append(str(task))
                                chek = True
                        log.info('Print Tasks')
                        if not check:
                            output.append('No such Task with such priority - {}!'.format(priority))
                            log.info('No such Task with such priority - {}!'.format(priority))
                    else:
                        output.append('Tasks:')
                        for task in tasklist:
                            output.append(str(task))
                        output.append('Task for which have access:')
                        log.info('Print Tasks')
                        for task in teamtasklist:
                            output.append(str(task))
        return '\n'.join(output)
