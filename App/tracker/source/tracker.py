from tracker.source.completetask import Completetask
from tracker.source.datab import Datab
from tracker.source.add import Add
from tracker.source.prints import Prints
from tracker.source.edit import Edit
from tracker.source.userf import Userf
from tracker.source.reccurent import Reccurent
from tracker.source.logger import Logger

class Tracker:
    """The main class of our library"""

    def __init__(self,config):
        """
        Initialising our programm class

        :param config: our cinfiguration file
        """
        
        self.config = Datab.configparse(config)
        self.logger = Logger.createlog(self.config)
        self.user = Datab.currentuser(self.config)
        self.tasklist, self.teamtasklist, self.userlist = Datab.load(self.config['Default']['database'], self.user)
        self.completetasklist, self.completeteamtasks, self.userlistct= Datab.load(self.config['Default']['complete'], self.user)

    def printtasks(self, priority, group, name, less):
        return Prints.printtask(self.tasklist, self.teamtasklist, self.completetasklist,
                                priority, group, name, less, self.logger)

    def createtask(self, name, group, priority, days, users, parent, deadline, list):
        self.tasklist, self.teamtasklist = Add.addtask(self.tasklist, self.teamtasklist, name, group, priority,
                                                       days, users, parent, deadline, list, self.logger, self.user)

    def removetask(self, name):
        self.tasklist, self.teamtasklist = Edit.removetask(self.tasklist, self.teamtasklist, name, self.logger)

    def edittask(self, name, newname, group, priority, dedlain, list, parrent):
        self.tasklist, self.teamtasklist = Edit.edittask(self.tasklist, self.teamtasklist, name, newname,
                                                         group, priority, dedlain, list, parrent, self.logger)

    def createusers(self, login):
        self.userlist, self.userlistct = Userf.createuser(self.userlist, self.userlistct, login, self.logger, self.config)

    def changeusers(self, login):
        Userf.changeuser(self.userlist, login, self.logger, self.config)

    def printusers(self):
        return Userf.printuser(self.userlist, self.logger, self.config)

    def removeuser(self, login):
        self.userlist, self.userlistct = Userf.deleteuser(self.userlist, self.userlistct, login, self.logger, self.config)

    def completetask(self, name):
        self.tasklist, self.completetasklist = Completetask.complete(self.tasklist, self.completetasklist, name, self.logger)

    def checkreccurent(self):
        self.tasklist, output = Reccurent.check(self.tasklist, self.logger)
        return output

    def checkfails(self):
        self.tasklist, output = Reccurent.checkfails(self.tasklist, '', self.logger)
        return output

    def saveall(self):
        Datab.save(self.tasklist, self.teamtasklist, self.config['Default']['database'], self.user, self.userlist)
        Datab.save(self.completetasklist, self.completeteamtasks, self.config['Default']['complete'], self.user, self.userlistct)
