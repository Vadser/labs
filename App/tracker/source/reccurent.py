import datetime

from tracker.source.state import State

class Reccurent:
    """Perform functions to upload our reccurent tasks and to check fails"""

    @staticmethod
    def check(tasklist, log):
        """
        Check reccurent tasks and, if it time to repeat it, create task with the same parameters
        """

        output = ''
        ischeck = False
        newtasks = []
        for tasks in tasklist:
            if tasks.reccurent:
                date = tasks.deadline
                day = datetime.datetime.now()
                if date <= day:
                    task = tasks
                    if isinstance(task.days,str):
                        task.deadline = date + datetime.timedelta(days = int(task.days))
                    else:
                        now = task.days[0]
                        task.days.remove(now)
                        now = now + datetime.timedelta(days = 7)
                        task.days.append(now)
                        task.days.sort()
                        task.deadline = task.days[0]
                    task.create = datetime.datetime.now()
                    task.change = datetime.datetime.now()
                    task.state = State.NOT_READY
                    output += 'Reccurent task {} was repeated!\n'.format(task.name)
                    log.info('Reccurent task {} was repeated!'.format(task.name))
                    newtasks.append(task)
                    ischeck = True
        for task in newtasks:
            tasklist.remove(task)
        if ischeck:
            tasklist = tasklist + newtasks
        return tasklist, output

    @staticmethod
    def failsubtasks(tasklist):
        """
        Change state on failed in each subtask of our failed task
        """

        for task in tasklist:
            task.state = State.FAILED
            task.subtasks = Reccurent.failsubtasks(task.subtasks)
        return tasklist

    @staticmethod
    def checkfails(tasklist, output, log):
        """
        Seaoch failed task and change state on failed if task was failed
        """

        for task in tasklist:
            if task.deadline is not None:
                date = task.deadline
                day = datetime.datetime.now()
                if date < day:
                    task.state = State.FAILED
                    output += 'Task {} was failed!\n'.format(task.name)
                    log.info('Task {} was failed!'.format(task.name))
                    task.subtasks = Reccurent.failsubtasks(task.subtasks)
                else:
                    task.subtasks, output = Reccurent.checkfails(task.subtasks, output, log)
        return tasklist, output
